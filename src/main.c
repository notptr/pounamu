/*The MIT License (MIT)

Copyright (c) <2016> <Matthew Deig>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#include <stdio.h>
#include <unistd.h>
#include <getopt.h>
#include <fcntl.h>

#include "markov_chain.h"

void scanfiles (char** argv);

int main(int argc, char **argv)
{
    char ch;
    int learn = 0;
    int chat = 0;
    

    while ((ch = getopt(argc, argv, "lc")) != -1)
    {
        switch (ch) 
        {
            case 'l':
                learn = 1;
                break;
            case 'c':
                chat = 1;
                break;
            default:
                fprintf (stderr, "usage: pounamu [-lc] [file]\n");
                return 1;
        }
    }

    if (!learn && !chat) 
    {
        fprintf (stderr, "usage: pounamu [-lc] [file]\n");
        return 1;
    }

    argv += optind;

    if (learn)
    {
        scanfiles (argv);
    }

    if (chat)
    {
        //do this later for now
    }



    return 0;
}


void scanfiles (char **argv)
{
    char* path;
    const char* filename;
    FILE* fp;
    int x = 0;

    while ((path = argv[x]) != NULL || x == 0)
    {
        if (path == NULL || strcmp (path, "-") == 0) 
        {
            filename = "stdin";
        }
        else
        {
            filename = path;
            fp = fopen(path, "r");
        }

        if (!fp || !strcmp(filename, "stdin"))
        {
            fprintf (stderr, "WARNING: %s\n", path);
        }
        else
        {
            //print out the files

            if (strcmp(filename, "stdin"))
            {
                //stdin stuff
            }
            else
            {
                fclose (fp);
            }
        }

        x++;
        
    }
}
