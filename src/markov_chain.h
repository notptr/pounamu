/*The MIT License (MIT)

Copyright (c) <2016> <Matthew Deig>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#ifndef MARKOV_CHAIN_H__
#define MARKOV_CHAIN_H__

#include <stdlib.h>
#include <string.h>

typedef struct mk_chain
{
    struct mk_chain* next;
    struct mk_chain* prev;
    float prob;
    unsigned hit;
    char* word;
} MK_CHAIN;

typedef struct mk_table
{
    struct mk_table* next;
    struct mk_table* prev;
    struct mk_chain* items;
    char* word;
} MK_TABLE;

// chain functions
void set_chain_prob (MK_CHAIN* chain);
void hit_chain (MK_CHAIN* chain);
void insert_chain (MK_CHAIN* chain, char* word);

unsigned sum_hits (MK_CHAIN* chain);
MK_CHAIN* get_chain_start (MK_CHAIN* chain);
MK_CHAIN* get_chain_end (MK_CHAIN* chain);
MK_CHAIN* find_chain (MK_CHAIN* chain, char* word);

//table functions
MK_TABLE* get_table_start (MK_TABLE* entry);
MK_TABLE* get_table_end (MK_TABLE* entry);
MK_TABLE* find_entry (MK_TABLE* entry, char* word);

void insert_entry (MK_TABLE* entry, char* word);
void insert_items (MK_TABLE* entry, char* word);



#endif
