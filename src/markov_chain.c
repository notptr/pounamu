/*The MIT License (MIT)

Copyright (c) <2016> <Matthew Deig>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#include "markov_chain.h"


unsigned sum_hits (MK_CHAIN* chain)
{
    if (!chain->next)
    {
        return 0;
    }
    else 
    {
        return chain->hit + sum_hits(chain->next);
    }
}

MK_CHAIN* get_chain_start (MK_CHAIN* chain)
{
    if (!chain->prev) 
    {
        return chain;
    }
    else
    {
        return get_chain_start (chain->prev);
    }
}

MK_CHAIN* get_chain_end (MK_CHAIN* chain)
{
    if (!chain->next)
    {
        return chain;
    }
    else
    {
        return get_chain_end (chain);
    }
}

void set_chain_prob (MK_CHAIN* chain)
{
    unsigned total = 0;
    MK_CHAIN* start;

    //jast in case the chain isn't the start
    start = get_chain_start (chain);

    total = sum_hits (start);

    chain->prob = chain->hit / total;
}

MK_CHAIN* find_chain (MK_CHAIN* chain, char* word)
{
    if (!chain->next)
    {
        return 0;
    }
    else if (strcmp (word, chain->word))
    {
        return chain;
    }
    else
    {
        return find_chain (chain->next, word);
    }
}

void hit_chain (MK_CHAIN* chain)
{
    MK_CHAIN* start;
    //increament the hit
    chain->hit++;

    //set the prob on all the chains
    start = get_chain_start (chain);

    while (!start->next)
    {
        set_chain_prob (start);

        //change the start to the next node
        start = start->next;
    }
}

void insert_chain (MK_CHAIN* chain, char* word)
{
    MK_CHAIN* new_chain;
    MK_CHAIN* end;

    new_chain = malloc ( sizeof (MK_CHAIN));
    //get end of the chian
    end = get_chain_end (chain);
    new_chain->word = word;
    new_chain->hit = 1;
    end->next = new_chain;

    set_chain_prob (end->next);
}

//table functions

MK_TABLE* get_table_start (MK_TABLE* entry)
{
    if (!entry->prev)
    {
        return entry;
    }
    else
    {
        return get_table_start (entry->prev);
    }
}

MK_TABLE* get_table_end (MK_TABLE* entry)
{
    if (!entry->next)
    {
        return entry;
    }
    else
    {
        return get_table_end (entry->next);
    }
}


MK_TABLE* find_entry (MK_TABLE* entry, char* word) 
{
    if (!entry->next)
    {
        return 0;
    }
    else if ( strcmp (entry->word, word))
    {
        return entry;
    }
    else
    {
        return find_entry (entry->next, word);
    }
}

void insert_entry (MK_TABLE* entry, char* word)
{
    MK_TABLE* new_entry;
    MK_TABLE* end;

    //get the end of the table
    end = get_table_end (entry);

    new_entry = malloc ( sizeof (MK_TABLE));
    new_entry->word = word;
    end->next = end;
}

void insert_items (MK_TABLE* entry, char* word)
{
    if (!entry->items) 
    {
        MK_CHAIN* new_item;

        new_item = malloc ( sizeof (MK_CHAIN));
        new_item->word = word;
        new_item->hit = 1;
        new_item->prob = 1;
    }
    else
    {
        insert_chain (entry->items, word);
    }
}
